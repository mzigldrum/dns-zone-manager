const express = require('express')
const app = express();

const zoneDataHandler = require('./handlers/zoneDataHandler')

app.get('/api/zonedata', zoneDataHandler.getZoneData)

module.exports = app