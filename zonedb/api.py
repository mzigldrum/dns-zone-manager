import json
import logging
import falcon
import gunicorn.app.base
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.util import deprecations
from zonedb.models import Zone, AuthToken, SchemeClaim, TrustList, TrustListCert
from zonedb.master import refresh_zonefile, reload_master
from datetime import datetime
import subprocess

deprecations.SILENCE_UBER_WARNING = 1
deprecations.SQLALCHEMY_WARN_20 = 0

def auth_zone(req):
    if not req.auth:
        raise falcon.HTTPForbidden("401 Unauthorized", "Authorization required")
    words = req.auth.split()
    if len(words) < 2:
        raise falcon.HTTPForbidden("401 Unauthorized", "Authorization required")
    if words[0].lower() != "bearer":
        raise falcon.HTTPForbidden("401 Unauthorized", "Authorization required")
    try:
        token = req.context.session.query(AuthToken).filter_by(token=words[1]).first()
        if token is None:
            logging.debug("Invalid token access request")
            raise falcon.HTTPForbidden("401 Unauthorized", "Invalid token")
    except NoResultFound:
        raise falcon.HTTPForbidden("401 Forbidden", "Authorization required")
    return token.zone


def load_json(req):
    if req.content_length > 0:
        try:
            return json.load(req.stream)
        except Exception as exc:
            raise falcon.HTTPBadRequest("400 Bad Request", "invalid body") from exc
    else:
        return dict()


def decode_certs(cert_list):
    if isinstance(cert_list, dict):
        cert_list = (cert_list,)
    if not isinstance(cert_list, (tuple, list)):
        raise falcon.HTTPBadRequest("400 Bad Request", "certificates must be a list")
    res = list()
    for cert in cert_list:
        usage = cert.get("usage")
        selector = cert.get("selector")
        matching = cert.get("matching")
        try:
            data = cert["data"]
        except KeyError as exc:
            raise falcon.HTTPBadRequest("400 Bad Request", f"'certificates': missing key '{exc}'")
        try:
            res.append(TrustListCert.create(usage, selector, matching, data))
        except ValueError as exc:
            raise falcon.HTTPBadRequest(f"400 Bad Request: {exc}")
    return res


class Status(object):
    def on_get(self, req, resp):
        zone = auth_zone(req)
        databaseStatus = "OK" # checked via auth_zone(req)
        system_status = "OK"
        doc = {
            "Zone": zone.apex,
            "status" : system_status,
            "dependencies" : {
                "Zone_Manager_server_status": "OK",
                "Database_status":databaseStatus,
                "NSD_server_status":"OK"
            }
        }

        status = subprocess.run(["service", "nsd", "status"], capture_output=True, text=True)
        if status.returncode == 0:       # nsd is running
            doc["dependencies"]["NSD_server_status"] = "OK"
        else:
            doc["status"] = "Dependency unavailable"
            doc["dependencies"]["NSD_server_status"] = "NSD server is not running"
        resp.body = json.dumps(doc, ensure_ascii=False)

class TrustListResource(object):
    def __init__(self, list_type):
        self.list_type = list_type

    def on_get(self, req, resp, scheme_name):
        zone = auth_zone(req)
        tl = (
            req.context.session.query(TrustList)
            .filter_by(zone=zone, list_type=self.list_type, name=scheme_name)
            .one_or_none()
        )
        if tl is None:
            resp.body = json.dumps([], ensure_ascii=False)
        else:
            resp.body = json.dumps({"did": tl.did}, ensure_ascii=False)

    def on_put(self, req, resp, scheme_name):
        zone = auth_zone(req)
        scheme_claim = (
            req.context.session.query(SchemeClaim)
            .filter_by(zone=zone, scheme=scheme_name)
            .one_or_none()
        )
        if scheme_claim is None:
            raise falcon.HTTPNotFound(title=scheme_name + " is not a valid trust scheme")
        if not zone.contains_name(scheme_name):
            logging.debug("TrustlistResource PUT, domain name not in zone. Not found: "+scheme_name)
            raise falcon.HTTPNotFound(title="domain name not in zone")
        data = load_json(req)
        try:
            did = data["did"]
        except KeyError as exc:
            raise falcon.HTTPBadRequest("400 Bad Request", f"missing key '{exc}'")
        if not did.startswith("did:"):
            raise falcon.HTTPBadRequest("400 Bad Request", "Request must contain a valid did")
        self.delete(req, zone, scheme_name)
        trust_list = TrustList(zone=zone, name=scheme_name, list_type=self.list_type, did=did)
        try:
            trust_list.rr()
        except:
            req.context.session.rollback()
            raise falcon.HTTPBadRequest("400 Bad Request", "invalid data in content")
        req.context.session.add(trust_list)
        req.context.session.commit()
        refresh_zonefile(req.context.session, req.context.environment, zone)
        reload_master(req.context.environment)
        logging.debug("Trust list record published. Scheme name: " + scheme_name)
        self.on_get(req, resp, scheme_name)
        logging.debug("TrustlistResource PUT, OK in scheme " + scheme_name)

    def on_delete(self, req, resp, scheme_name):
        zone = auth_zone(req)
        if self.delete(req, zone, scheme_name):
            resp.status = falcon.HTTP_204
            logging.debug("Trust list record deleted. Scheme name: " + scheme_name)
        else:
            resp.status = falcon.HTTP_404

    def delete(self, req, zone, scheme_name):
        """Deletes a trust list entry.
        Returns whether there was an entry to delete.
        """
        tl = (
            req.context.session.query(TrustList)
            .filter_by(zone=zone, name=scheme_name, list_type=self.list_type)
            .one_or_none()
        )
        if tl is None:
            return False
        else:
            req.context.session.delete(tl)
            req.context.session.commit()
            refresh_zonefile(req.context.session, req.context.environment, zone)
            reload_master(req.context.environment)
            return True


class SchemeClaimResource(object):
    def on_get(self, req, resp, scheme_name):
        zone = auth_zone(req)
        claims = (
            req.context.session.query(SchemeClaim)
            .filter_by(zone=zone, name=scheme_name)
            .all()
        )
        if claims is None:
            logging.debug("SchemeClaimResource GET, empty")
            resp.body = json.dumps([], ensure_ascii=False)
        else:
            doc = dict(schemes=[])
            for item in claims:
                doc["schemes"].append(item.scheme)
            logging.debug("SchemeClaimResource GET, OK. " + scheme_name)
            resp.body = json.dumps(doc, ensure_ascii=False)

    def on_put(self, req, resp, scheme_name):
        zone = auth_zone(req)
        data = load_json(req)
        try:
            schemes = data["schemes"]
        except KeyError as exc:
            raise falcon.HTTPBadRequest("400 Bad Request", f"missing key '{exc}'")
        if not isinstance(schemes, (list, tuple)):
            raise falcon.HTTPBadRequest("400 Bad Request", "'schemes' must be a list")
        for item in schemes:
            if not isinstance(item, str):
                raise falcon.HTTPBadRequest("400 Bad Request", "'schemes' must be list of strings")
            try:
                item.encode("ascii")
            except:
                raise falcon.HTTPBadRequest("400 Bad Request", "'schemes' must be domain names")
            if len(item) > 255:
                raise falcon.HTTPBadRequest("400 Bad Request", "'schemes' must be domain names")
        req.context.session.query(SchemeClaim).filter_by(
            zone=zone, name=scheme_name
        ).delete()
        for item in schemes:
            claim = SchemeClaim(zone=zone, name=scheme_name, scheme=item)
            try:
                claim.rr()
            except:
                req.context.session.rollback()
                raise falcon.HTTPBadRequest("400 Bad Request", "invalid data in content")
            req.context.session.add(claim)
        req.context.session.commit()
        refresh_zonefile(req.context.session, req.context.environment, zone)
        reload_master(req.context.environment)
        logging.debug("Trust scheme record published. Scheme name: " + scheme_name)
        self.on_get(req, resp, scheme_name)

    def on_delete(self, req, resp, scheme_name):
        zone = auth_zone(req)
        res = (
            req.context.session.query(SchemeClaim)
            .filter_by(zone=zone, name=scheme_name)
            .delete()
        )
        req.context.session.commit()
        refresh_zonefile(req.context.session, req.context.environment, zone)
        reload_master(req.context.environment)
        if res == 0:
            resp.status = falcon.HTTP_404
        else:
            resp.status = falcon.HTTP_204
            logging.debug("Trust scheme record deleted. Scheme name: " + scheme_name)



class ViewZone(object):
    def on_get(self, req, resp):
        zone = auth_zone(req)
        zones = req.context.session.query(Zone).all()
        all_zones = []
        for zone in zones:
            current_zone = {"id": zone.id, "apex": zone.apex, "schemes": []}
            schemes = (
                req.context.session.query(SchemeClaim).filter_by(zone_id=zone.id).all()
            )
            trust_lists = req.context.session.query(TrustList).all()
            unique_schemes = set(scheme.name for scheme in schemes)
            for unique_scheme in unique_schemes:
                top_scheme = {"name": unique_scheme, "subSchemes": []}
                for scheme in schemes:
                    if scheme.name == unique_scheme:
                        subscheme = scheme.scheme
                        did = ""
                        for tl in trust_lists:
                            if tl.name == subscheme:
                                did = tl.did
                        el = {"subscheme": subscheme, "trustListDid": did}
                        top_scheme["subSchemes"].append(el)
                current_zone["schemes"].append(top_scheme)
            all_zones.append(current_zone)
        responseObject = {"zones": all_zones}
        resp.body = json.dumps(responseObject, ensure_ascii=False)


class ApiApplication(gunicorn.app.base.BaseApplication):
    def __init__(self, options):
        self.options = options
        super(ApiApplication, self).__init__()

    def load_config(self):
        self.cfg.set("bind", self.options.bind)

    def load(self):
        api = falcon.API(
            request_type=type(
                "Request", (falcon.Request,), dict(context_type=lambda rq: self.options)
            )
        )

        api.add_route("/status", Status())
        api.add_route("/names/{scheme_name}/trust-list", TrustListResource("scheme"))
        api.add_route("/names/{scheme_name}/schemes", SchemeClaimResource())
        api.add_route("/view-zone", ViewZone())

        return api
