SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
ROOT_PATH=$(dirname $(dirname "$SCRIPT_PATH"))
DEPLOY_PATH="$ROOT_PATH/deployment/local"

# build dns docker image
if [ $1 = "build" ]; then
  echo "Switching context to dns building... ($ROOT_PATH)"
  cd "$ROOT_PATH"
  docker build -t zonemanager-nsd:latest .

  # build ui docker image
  echo "Switching context to ui building... ($ROOT_PATH/ui)"
  cd "$ROOT_PATH/ui"
  docker build -t zonemanager-ui:latest .
else 
  echo "No build flag. Skipping build. To build images run this script as 'deploy.sh build'"
fi

echo "Docker image building has been finished. Starting dns deployment."

echo "Switching context to deployment ($DEPLOY_PATH)"
cd "$DEPLOY_PATH"
docker compose up -d dns-server

echo "----------------------------------------------------"
echo "DNS has been deployed. Please get the API key and paste it here!"
read -p "API_KEY: " API_KEY
echo "Thank you. Configuring ui deployment now."

echo "API_KEY=$API_KEY
ZONEMGR_URL=http://zonemanager-nsd:16001
" > ./ui-local.env

echo "Configured env variables for ui. Starting..."
docker compose up -d ui-server
